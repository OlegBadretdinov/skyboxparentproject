//
//  ViewController.swift
//  SkyboxParentApplication
//
//  Created by Oleg Badretdinov on 04.06.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import SkyboxGraph

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Graph", let dest = segue.destination as? SBGraphViewController {
            dest.isin = "1"
            dest.serverUrl = "https://glacial-escarpment-83262.herokuapp.com"
        }
    }
}

